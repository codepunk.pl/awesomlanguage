﻿using Antlr4.Runtime.Misc;
using ConsoleApp1.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class SimpleVisitor : SimpleBaseVisitor<object>
    {
        private readonly OutputHandler outputHandler;

        private readonly Dictionary<string, SFunction> HeapFunctions = new Dictionary<string, SFunction>();
        private readonly Dictionary<int, Dictionary<string, object>> StackVariables = new Dictionary<int, Dictionary<string, object>>();
        private int CurrentStackId = 0;

        public SimpleVisitor(OutputHandler outputHandler) {
            this.outputHandler = outputHandler;
            HeapFunctions["Write"] = SFunction.Wrap<string>(outputHandler.OnWriteLine);
            StackVariables[0] = new Dictionary<string, object>();
        }

        public override object VisitAssignment([NotNull] SimpleParser.AssignmentContext context)
        {
            var varName = context.IDENTIFIER().GetText();
            var value = Visit(context.expression());
            StackVariables[CurrentStackId][varName] = value;

            return null;
        }

        public override object VisitConstant([NotNull] SimpleParser.ConstantContext context)
        {
            if (context.INTEGER() != null)
            {
                return int.Parse(context.INTEGER().GetText());
            }

            if (context.STRING() != null)
            {
                return context.STRING().GetText()[1..^1];
            }

            if (context.BOOL() != null)
            {
                return bool.Parse(context.BOOL().GetText());
            }

            if (context.NULL() != null)
            {
                return null;
            }

            throw new Exception($"Invalid constant {context.GetText()}.");
        }

        public override object VisitIdentifierExpression([NotNull] SimpleParser.IdentifierExpressionContext context)
        {
            var varName = context.IDENTIFIER().GetText();
            if (StackVariables[CurrentStackId].ContainsKey(varName))
            {
                return StackVariables[CurrentStackId][varName];
            }

            return null;
            //throw new Exception($"Variable {varName} is not defined.");
        }

        public override object VisitAdditiveExpression([NotNull] SimpleParser.AdditiveExpressionContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            var op = context.addOp().GetText();
            return op switch
            {
                "+" => Add(left, right),
                "-" => Substract(left, right),
                _ => throw new Exception($"Operation {op} not recognized.")
            };
        }

        public override object VisitMultiplicatuveExpression([NotNull] SimpleParser.MultiplicatuveExpressionContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            var op = context.multOp().GetText();
            return op switch
            {
                "*" => Multiply(left, right),
                "/" => Divide(left, right),
                _ => throw new Exception($"Operation {op} not recognized.")
            };
        }

        public override object VisitWhileBlock([NotNull] SimpleParser.WhileBlockContext context)
        {            
            while(IsTrue(Visit(context.expression())))
            {
                Visit(context.block());
            }

            return null;
        }

        public override object VisitComparisonExpression([NotNull] SimpleParser.ComparisonExpressionContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            var op = context.compareOp().GetText();
            return op switch
            {
                "==" => IsEqual(left, right),
                "!=" => IsNotEqual(left, right),
                ">" => IsGreaterThan(left, right),
                "<" => IsLessThan(left, right),
                ">=" => IsEqualOrGreaterThan(left, right),
                "<=" => IsEqualOrLessThan(left, right),
                _ => throw new Exception($"Operation {op} not recognized.")
            };
        }

        public override object VisitReturn([NotNull] SimpleParser.ReturnContext context)
        {
            var result = Visit(context.expression());
            StackVariables[CurrentStackId]["#result"] = result;
            return null;
        }

        public override object VisitFunctionCall([NotNull] SimpleParser.FunctionCallContext context)
        {
            var name = context.IDENTIFIER().GetText();
            var args = context.expression().Select(Visit).ToArray();

            if (!HeapFunctions.ContainsKey(name))
                throw new Exception($"Function {name} is not defined.");

            if (HeapFunctions[name] is SFunction func)
            {
                try
                {
                    CurrentStackId++;
                    StackVariables[CurrentStackId] = new Dictionary<string, object>();
                    func.invoke(this, StackVariables[CurrentStackId], args);                    
                    return StackVariables[CurrentStackId].GetValueOrDefault("#result", null);
                } finally
                {
                    StackVariables.Remove(CurrentStackId);
                    CurrentStackId--;
                }
            }

            throw new Exception($"Function {name} is not defined.");
        }

        public override object VisitFunctionDefinition([NotNull] SimpleParser.FunctionDefinitionContext context)
        {
            var name = context.IDENTIFIER().GetText();
            var argsLiteral = context.arguments()?.GetText();            
            var block = context.block();
            string[] args;
            if (argsLiteral.Length == 0)
                args = null;
            else
                args = argsLiteral.Split(",");

            if (HeapFunctions.ContainsKey(name))
                throw new Exception($"Function {name} already exists.");

            HeapFunctions[name] = SFunction.FromBlock(name, args, block);

            return null;
        }

        private object IsEqual(object left, object right)
        {
            if (left is int && right is int)
                return (int)left == (int)right;

            throw new Exception($"Cannot compare values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object IsNotEqual(object left, object right)
        {
            if (left is int && right is int)
                return (int)left != (int)right;

            throw new Exception($"Cannot compare values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object IsGreaterThan(object left, object right)
        {
            if (left is int && right is int)
                return (int)left > (int)right;

            throw new Exception($"Cannot compare values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object IsLessThan(object left, object right)
        {
            if (left is int && right is int)
                return (int)left < (int)right;

            throw new Exception($"Cannot compare values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object IsEqualOrGreaterThan(object left, object right)
        {
            if (left is int && right is int)
                return (int)left >= (int)right;

            throw new Exception($"Cannot compare values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object IsEqualOrLessThan(object left, object right)
        {
            if (left is int && right is int)
                return (int)left <= (int)right;

            throw new Exception($"Cannot compare values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private bool IsTrue(object value)
        {
            if (value is bool b)
                return b;

            throw new Exception("Value is not boolean.");
        }

        private object Add(object left, object right)
        {
            if (left is int && right is int)
                return (int)left + (int)right;

            if (left is String || right is String)
                return $"{left}{right}";

            throw new Exception($"Cannot add values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object Substract(object left, object right)
        {
            if (left is int && right is int)
                return (int)left - (int)right;

            throw new Exception($"Cannot substract values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object Multiply(object left, object right)
        {
            if (left is int && right is int)
                return (int)left * (int)right;

            throw new Exception($"Cannot multiply values of types {left?.GetType()} and {right?.GetType()}.");
        }

        private object Divide(object left, object right)
        {
            if (left is int && right is int)
                return (int)left / (int)right;

            throw new Exception($"Cannot divide values of types {left?.GetType()} and {right?.GetType()}.");
        }
    }
}
