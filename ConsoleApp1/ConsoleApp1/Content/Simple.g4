﻿grammar Simple;

program: line* EOF;

line: functionDefinition | statement | ifBlock | whileBlock;

statement: (assignment|functionCall|return) ';';

functionDefinition: 'fun' IDENTIFIER '(' arguments ')' block;

ifBlock: 'if' expression block ('else' elseIfBlock)?;

elseIfBlock: block | ifBlock;

whileBlock: WHILE expression block;

WHILE: 'while';

assignment: IDENTIFIER '=' expression;

return: 'return' expression;

functionCall: IDENTIFIER '(' (expression (',' expression)*)? ')';

expression
	: constant										#constantExpression
	| IDENTIFIER									#identifierExpression
	| '(' expression ')'							#parenthesizedExpression
	| '!' expression								#notExpression
	| expression multOp expression					#multiplicatuveExpression
	| expression addOp expression					#additiveExpression
	| expression compareOp expression				#comparisonExpression
	| expression boolOp expression					#booleanExpression
	| functionDefinition							#functionDefinitionExpression
	| functionCall									#functionCallExpression
	;

arguments: (expression (',' expression)*)?;

multOp: '*' | '/' | '%';
addOp: '+' | '-';
compareOp: '==' | '!=' | '>' | '<' | '>=' | '<=';
boolOp: BOOL_OPERATOR;

BOOL_OPERATOR: 'and' | 'or' | 'xor';

constant: INTEGER | STRING | BOOL | NULL;

INTEGER: [0-9]+;
FLOAT: [0-9]+ '.' [0-9]+;
STRING: ('"' ~'"'* '"') | ('\'' ~'\''* '\'');
BOOL: 'true' | 'false';
NULL: 'null';

block: '{' line* '}';

WS: [ \t\r\n]+ -> skip;

IDENTIFIER: [a-zA-Z_][a-zA-Z0-9_]*;