﻿using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using ConsoleApp1.Content;
using System;
using System.IO;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var filename = "Content/test.ss";
            var fileContents = File.ReadAllText(filename);
            var inputStream = new AntlrInputStream(fileContents);
            var lexer = new SimpleLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(lexer);
            var parser = new SimpleParser(commonTokenStream);
            //parser.AddErrorListener(new );
            var context = parser.program();
            var outputHandler = new MainOutputHandler();
            var visitor = new SimpleVisitor(outputHandler);
           // var walker = new ParseTreeWalker();
            //walker.Walk(new Mylistener(), context);
            visitor.Visit(context);
        }

        public static int ExecuteCode(string code, OutputHandler output)
        {
            var inputStream = new AntlrInputStream(code);
            var lexer = new SimpleLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(lexer);
            var parser = new SimpleParser(commonTokenStream);
            var context = parser.program();
            var visitor = new SimpleVisitor(output);
            visitor.Visit(context);
            return 0;
        }


        private class MainOutputHandler : OutputHandler
        {
            public void OnWriteLine(string line)
            {
                Console.WriteLine(line);
            }
        }
    }
}