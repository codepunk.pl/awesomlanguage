﻿using ConsoleApp1.Content;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class SFunction
    {

        internal static SFunction Wrap<T>(Action<T> remoteCall)
        {
            return new SFunction(t => { remoteCall((T)t[0]); return true; });
        }

        public static SFunction FromBlock(string name, string[] args, SimpleParser.BlockContext block)
        {            
            return new SFunction(name, args, block);
        }

        private readonly Func<object[], object> function;
        private readonly string name;
        private readonly string[] args;
        private readonly SimpleParser.BlockContext block;


        public SFunction(Func<object[], object> function)
        {
            this.function = function;
        }


        public SFunction(string name, string[] args, SimpleParser.BlockContext block)
        {
            this.name = name;
            this.args = args;
            this.block = block;
        }      

        public object invoke(SimpleVisitor simpleVisitor, Dictionary<string, object> stackVariables, object[] argsData)
        {
            if (function != null)
            {
                return function(argsData);
            }
            if (name != null && args != null && block != null)
            {
                if (args.Length != argsData.Length)
                {
                    throw new Exception($"Arguments do not match function {name} arguments.");
                }
                for (int i=0;i< args.Length;i++)
                {
                    stackVariables[args[i]] = argsData[i];
                }

                return simpleVisitor.Visit(block);
            }
            if (name != null && block != null)
            {
                return simpleVisitor.Visit(block);
            }

            throw new Exception("Unable to invoke function");
        }
    }
}
