﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public interface OutputHandler
    {
        public void OnWriteLine(String line);
    }
}
